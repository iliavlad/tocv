# tocv

Server part of Rem-Inform system ( https://inform.rem-prim.ru )

It's the monitoring system to use with different heat-, electro- and watermeters.

Server part is used to work with remote devices via modems. Modems are configured to connect to internet server and keep connections alive.
Server listen, accept and make read/write stream operations with modem connections.
In other hand server checks type of device connected to modem from database and create Driver. Driver implements communication protocol provided by meter vendors.
Different datas from devices stored in database and used by end customers (tables, graphs, analyses).
