<?php

namespace Inform\Server\Loop;

use ExtEvent\Event as Event;
use ExtEvent\EventBase as EventBase;
use Inform\Server\Stream\ListenerInterface;

/**
 * Работа с библиотеков libevent
 * Основан на коде ReactPHP (https://github.com/reactphp/event-loop/blob/master/src/ExtEventLoop.php)
 */
class ExtEvent implements CommandInterface, StreamInterface
{
    /**
     * Признак необходимости остановки сервера
     *
     * @var bool
     */
    private $serverShouldStop = false;

    /**
     * Работа с событиями
     *
     * @var ExtEvent\EventBase
     */
    private $eventBase;

    /**
     * Список потоков для чтения (в том числе приём сообщения на слушающих потоках)
     *
     * @var array
     */
    private $eventReadStreams = [];

    /**
     * Список потоков для записи
     *
     * @var array
     */
    private $eventWriteStreams = [];

    /**
     * Коллбек на события
     *
     * @var callable
     */
    private $streamCallback;

    public function __construct()
    {
        $this->eventBase = new EventBase();

        $this->streamCallback = function ($stream, $flags, ListenerInterface $listener) {
            if (Event::READ === (Event::READ & $flags)) {
                $listener->onReadData();
            } else if (Event::WRITE === (Event::WRITE & $flags)) {
                $listener->onWriteData();
            } else if (Event::TIMEOUT === (Event::TIMEOUT & $flags)) {
                $listener->onTimeout();
            }
        };
    }

    /**
     * @inheritedDoc
     */
    public function mainCycle()
    {
        while (true != $this->serverShouldStop) {
            $flags = EventBase::LOOP_ONCE;
            if (false != $this->serverShouldStop) {
                $flags |= EventBase::LOOP_NONBLOCK;
            }

             $this->eventBase->loop($flags);
        }
    }

    /**
     * @inheritedDoc
     */
    public function stop()
    {
        $this->serverShouldStop = true;
    }

    /**
     * @inheritedDoc
     */
    public function addReadStream($stream, ListenerInterface $listener, $timeout = null)
    {
        $key = (int) $stream;
        if (!isset($this->eventReadStreams[$key])) {
            $event = new Event(
                $this->eventBase,
                $stream,
                Event::PERSIST | Event::READ,
                $this->streamCallback,
                $listener
            );

            if (null === $timeout) { // слушаюшие потоки работают всегда, не прерываются по timeout
                $event->add();
            } else {
                $event->add($timeout);
            }

            // ext-event does not increase refcount on stream resources for PHP 7+
            // manually keep track of stream resource to prevent premature garbage collection
            $this->eventReadStreams[$key] = [
                'event' => $event
                , 'ref' => $stream
            ];
        }

        return $this;
    }

    /**
     * @inheritedDoc
     */
    public function addWriteStream($stream, ListenerInterface $listener)
    {
        $key = (int) $stream;
        if (!isset($this->eventWriteStreams[$key])) {
            $event = new Event(
                $this->eventBase,
                $stream,
                Event::PERSIST | Event::WRITE,
                $this->streamCallback,
                $listener
            );

            $event->add();
            // ext-event does not increase refcount on stream resources for PHP 7+
            // manually keep track of stream resource to prevent premature garbage collection
            $this->eventWriteStreams[$key] = [
                'event' => $event
                , 'ref' => $stream
            ];
        }

        return $this;
    }

    /**
     * @inheritedDoc
     */
    public function removeReadStream($stream)
    {
        $key = (int) $stream;
        if (isset($this->eventReadStreams[$key])) {
            $this->eventReadStreams[$key]['event']->free();
            unset(
                $this->eventReadStreams[$key]['event']
                , $this->eventReadStreams[$key]['ref']
                , $this->eventReadStreams[$key]
            );
        }

        return $this;
    }

    /**
     * @inheritedDoc
     */
    public function removeWriteStream($stream)
    {
        $key = (int) $stream;
        if (isset($this->eventWriteStreams[$key])) {
            $this->eventWriteStreams[$key]['event']->free();
            unset(
                $this->eventWriteStreams[$key]['event']
                , $this->eventWriteStreams[$key]['ref']
                , $this->eventWriteStreams[$key]
            );
        }

        return $this;
    }
}
