<?php

namespace Inform\Server\Loop;

/**
 * Методы управления циклом обработки событий
 */
interface CommandInterface
{
    /**
     * Основной цикл, в котором ожидаем события чтения-записи-таймаута
     */
    public function mainCycle();

    /**
     * Остановка сервера
     */
    public function stop();
}
