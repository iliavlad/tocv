<?php

namespace Inform\Server\Loop;

use Inform\Server\Stream\ListenerInterface;

/**
 * Методы добавления/удаления потоков из список ожидания событий чтения/записи
 */
interface StreamInterface
{
    /**
     * Добавление потока в список ожидающих события чтения
     *
     * @param resource $stream Поток
     * @param ListenerInterface $listener Обработчик
     * @param null|integer $timeout Таймаут
     * @return void
     */
    public function addReadStream($stream, ListenerInterface $listener, $timeout = null);

    /**
     * Добавление потока в список ожидающих готовности к записи
     *
     * @param resource $stream Поток
     * @param ListenerInterface $listener Обработчик
     * @return void
     */
    public function addWriteStream($stream, ListenerInterface $listener);

    /**
     * Удаление потока из списка ожидающих события чтения
     *
     * @param resource $stream Поток
     * @return void
     */
    public function removeReadStream($stream);

    /**
     * Удаление потока из списка ожидающих события записи
     *
     * @param resource $stream Поток
     * @return void
     */
    public function removeWriteStream($stream);
}
