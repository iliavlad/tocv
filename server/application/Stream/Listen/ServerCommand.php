<?php

namespace Inform\Server\Stream\Listen;

use Inform\Server\Loop\StreamInterface as LoopStreamInterface;
use Inform\Server\Server\CommandInterface;
use Inform\Server\Stream\Connected\ServerCommand as ConnectedStream;

/**
 * Слушающий поток, который работает с командами сервера
 */
class ServerCommand extends Stream
{
    /**
     * Сервер команд
     *
     * @var CommandInterface
     */
    private $serverCommand;

    /**
     *
     * @param LoopStreamInterface $events Работа с событиями
     * @param string $serverAddress Адрес сервера
     * @param string $portToListen Порт
     * @param CommandInterface $serverCommand Сервер команд
     */
    public function __construct(LoopStreamInterface $events, $serverAddress, $portToListen,
        CommandInterface $serverCommand)
    {
        parent::__construct($events, $serverAddress, $portToListen);

        $this->serverCommand = $serverCommand;
    }

    public function __destruct()
    {
        $this->serverCommand = null;

        parent::__destruct();
    }

    /**
     * @inheritedDoc
     */
    protected function createConnectedStream($socketAccepted, array $exchangeData)
    {
        return new ConnectedStream($this->events, $socketAccepted, $this, $exchangeData, $this->serverCommand);
    }
}
