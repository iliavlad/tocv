<?php

namespace Inform\Server\Stream\Listen;

use Inform\Server\Stream\Stream as BaseStream;
use Inform\Server\Loop\StreamInterface as LoopStreamInterface;
use Inform\Server\Log;

/**
 * Слушающий поток, который может принимать соединение и подтверждать его
 *   Когда клиент успешно подключён, создаётся нужный подключённый поток
 *     для обработки команд сервера или работы с конечными приборами
 */
abstract class Stream extends BaseStream implements StreamInterface
{
    /**
     * Подключённый поток
     *
     * @var \Inform\Server\Stream\Connected\Stream
     */
    protected $connectedStream;

    /**
     * Порт, который прослушивает поток
     *
     * @var string
     */
    private $port = '';

    /**
     * Данные об удалённом подключении
     *
     * @var string
     */
    private $remoteIpPort = '';

    /**
     * Время подключения
     *
     * @var int
     */
    private $connectedTime = 0;

    /**
     *
     * @param LoopStreamInterface $events Работа с событиями
     * @param string $serverAddress Адрес сервера
     * @param string $portToListen Порт для прослушивания
     */
    public function __construct(LoopStreamInterface $events, $serverAddress, $portToListen)
    {
        parent::__construct($events);

        $this->port = $portToListen;

        $this->listenPort($serverAddress, $portToListen);
    }

    public function __destruct()
    {
        $this->closeClient('Закрытие слушающего потока');

        parent::__destruct();
    }

    /**
     * Принимает соединение и создаём подключенный поток
     */
    public function onReadData()
    {
        // если на этом порту уже есть соединение, то закрываем его. пока 1 соединение за раз.
        $this->closeClient('Новое соединение');

        $socketAccepted = @stream_socket_accept($this->socket);
        if (false === $socketAccepted) {
            $error = error_get_last();
            Log::log(SERVERLOGNAME,
                "ERROR Server streamAcceptSocket stream_socket_accept {$this->port} " .
                    var_export(error_get_last(), true));
        } else {
            $remoteIpPort = @stream_socket_get_name($socketAccepted, true);
            // Подавление ошибки "Transport endpoint is not connected"
            if (false === $remoteIpPort) {
                $error = socket_last_error();
                Log::log(SERVERLOGNAME,
                    "ERROR Server streamAcceptSocket ошибка приёма соединения {$this->port} " .
                    "error_get_last() " . var_export(error_get_last(), true) . "\n" .
                    "socket_last_error() error $error " . socket_strerror($error));

                $this->closeSocket($socketAccepted);
            } else {
                stream_set_blocking($socketAccepted, false);

                $this->remoteIpPort = $remoteIpPort;
                $this->connectedTime = time();
                $this->connectedStream = $this->createConnectedStream($socketAccepted,
                    [
                        'ip' => $this->remoteIpPort,
                        'port' => $this->port,
                        'connected_time' => $this->connectedTime,
                    ]);

                Log::log(SERVERLOGNAME, "Server streamAcceptSocket port {$this->port} ip $remoteIpPort");
            }
        }
    }

    /**
     * @inheritedDoc
     */
    public function closeClient($reason = '')
    {
        if ($this->connectedStream) {
            Log::log(SERVERLOGNAME,
                "Server close port {$this->port} remote port:ip $this->remoteIpPort Причина {$reason}" .
                ' Был на связи ' . $this->getGraceTime(time() - $this->connectedTime));

            // поток, который соединился с данным и ждёт чтения-записи-закрытия
            $this->connectedStream->close($reason);
            $this->connectedStream = null;
            $this->connectedTime = 0;
        }
    }

    /**
     * Порт, который прослушиваем
     *
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Создание присоединённого потока
     *
     * @param resource $socketAccepted
     * @param array $exchangeData Данные для Обмена
     * @return \Inform\Server\Stream\Connected\Stream
     */
    abstract protected function createConnectedStream($socketAccepted, array $exchangeData);

    /**
     * Сокетные функции для работы прослушивания порта
     *
     * @param string $serverAddress Адрес сервера
     * @param string $port Порт
     * @return resource
     * @throws \Exception
     */
    private function listenPort($serverAddress, $port)
    {
        $errno = -1;
        $errstr = '';

        $socketListen = @stream_socket_server(
            ($serverAddress . ':' . $port),
            $errno,
            $errstr,
            STREAM_SERVER_BIND | STREAM_SERVER_LISTEN
        );

        if (false === $socketListen) {
            throw new \Exception("Ошибка создания слушателя для порта $port, причина ($errno: $errstr)");
        }

        stream_set_blocking($socketListen, false);

        $this->events->addReadStream($socketListen, $this);

        $this->socket = $socketListen;

        return $socketListen;
    }

    /**
     * Получение красивого вида количества секунд соединения
     *
     * @param int $seconds
     * @return string
     */
    private function getGraceTime($seconds)
    {
        $h = (int)($seconds / 60 / 60);
        $i = (int)(($seconds - ($h * 60 * 60)) / 60);
        $s = $seconds % 60 % 60;

        return "$h:$i:$s (ч:м:с)";
    }
}
