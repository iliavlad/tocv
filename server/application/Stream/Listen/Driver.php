<?php

namespace Inform\Server\Stream\Listen;

use Inform\Server\Stream\Connected\Driver as ConnectedStream;

/**
 * Слушающий поток, который работает с конечными приборами
 */
class Driver extends Stream
{
    /**
     * @inheritedDoc
     */
    protected function createConnectedStream($socketAccepted, array $exchangeData)
    {
        return new ConnectedStream($this->events, $socketAccepted, $this, $exchangeData);
    }

    /**
     * Получение статуса клиента
     *
     * @return string
     */
    public function getClientStatus()
    {
        $status = \Inform\Server\Exchange\Exchange::EXCHANGE_STATUS_NOT_ONLINE;

        if ($this->connectedStream) {
            $status = $this->connectedStream->getClientStatus();
        }

        return $status;
    }

    /**
     * Отправка команды клиенту
     *
     * @param array $parts Команда
     */
    public function sendCommandToClient(array $parts)
    {
        if ($this->connectedStream) {
            $this->connectedStream->sendCommandToClient($parts);
        }
    }
}
