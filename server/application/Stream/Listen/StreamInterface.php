<?php

namespace Inform\Server\Stream\Listen;

/**
 * Работа со слушающим потоком из подключённого потока
 */
interface StreamInterface
{
    /**
     * Закрытие клиентом соединения при ошибке или timeout
     *
     * @param string $reason
     */
    public function closeClient($reason = '');
}
