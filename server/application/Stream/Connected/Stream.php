<?php

namespace Inform\Server\Stream\Connected;

use Inform\Server\Stream\Stream as BaseStream;
use Inform\Server\Loop\StreamInterface as LoopStreamInterface;
use Inform\Server\Stream\Listen\StreamInterface as ListenStreamInterface;
use Inform\Server\Exchange\Exchange;
use Inform\Server\Log;

/**
 * Подключённый поток
 *   Выполняет общие функции работы с подключённым сокетом
 */
abstract class Stream extends BaseStream implements StreamInterface
{
    /**
     * Таймаут соединения
     *   При отсутствии чтения из потока будет вызван таймаут-коллбек
     *
     * @var int
     */
    protected $connectionTimeout = 70;

    /**
     * Время, в течение которого не было чтения
     *   Обнуляется при чтении из потока, увеличивается при таймауте, передаётся в Обмен для анализа
     *
     * @var int
     */
    protected $noReadTime = 0;

    /**
     * Слушающий поток
     *
     * @var ListenStreamInterface
     */
    protected $listenStream;

    /**
     * Обработка сообщений
     *
     * @var \Inform\Server\Exchange\Exchange
     */
    protected $exchange;

    /**
     * Статус Обмена
     *
     * @var string
     */
    protected $exchangeStatus;

    /**
     *
     * @var int
     */
    protected $exchangeStatusUpdated;

    /**
     * Строки, которые записывает Обмен
     * 
     *   Он может попросить записать несколько строк до того, как записались предыдущие.
     *   Поэтому храним все в массиве
     *
     * @var array of strings
     */
    private $stringsToWrite = [];

    /**
     *
     * @param LoopStreamInterface $events Работа с событиями
     * @param resource $socket Сокет
     * @param ListenStreamInterface $listenStream Работа со слушающим потоком
     * @param array $exchangeData Данные для Обмена
     */
    public function __construct(LoopStreamInterface $events,
        $socket,
        ListenStreamInterface $listenStream,
        array $exchangeData)
    {
        parent::__construct($events);

        $this->socket = $socket;

        $this->listenStream = $listenStream;

        $this->events->addReadStream($socket, $this, $this->connectionTimeout);

        $this->exchange = $this->createExchange($exchangeData);

        $this->exchangeStatus = Exchange::EXCHANGE_STATUS_ONLINE;
        $this->exchangeStatusUpdated = time();
    }

    public function __destruct()
    {
        $this->close();

        parent::__destruct();
    }

    /**
     * Вызывается из EventLoop как функция вызова на событие чтения из сокета
     */
    public function onReadData()
    {
        // Подавление сообщения "Connection reset by peer" в php_errors.log
        $message = @stream_socket_recvfrom($this->socket, 4096);

        if ('' !== $message && false !== $message) {
            if (null === $this->exchange) {
                Log::log(SERVERLOGNAME, 'ERROR Server Чтение из сокета, для которого не установлен Exchange');
            } else {
                $this->noReadTime = 0;
                $this->exchangeStatusUpdated = time();
                $this->exchange->onReadMessage($message);
            }
        }

        if ('' === $message || false === $message || !is_resource($this->socket)) { // данных от клиента больше не будет
            $this->listenStream->closeClient('закрытие клиентом');
        }

    }

    /**
     * Вызывается из EventLoop как функция вызова на событие записи в сокет
     */
    public function onWriteData()
    {
        $stringToWrite = $this->stringsToWrite[0];

        // Подавление ошибки "Connection reset by peer"
        if (
                !is_resource($this->socket)
                || false === ($sentByteCount = @stream_socket_sendto($this->socket, $stringToWrite))
            ) {
            $error = error_get_last();
            $this->listenStream->closeClient('закрытие клиентом streamsWriteToSocket type ' .
                $error['type'] . ' message ' . trim($error['message']));
        } else {
            $this->exchangeStatusUpdated = time();

            $this->stringsToWrite[0] = (string) substr($this->stringsToWrite[0], $sentByteCount);

            if (0 === strlen($this->stringsToWrite[0])) {
                array_shift($this->stringsToWrite);
            }

            if (0 == count($this->stringsToWrite)) {
                $this->events->addReadStream($this->socket, $this, $this->connectionTimeout);
                $this->events->removeWriteStream($this->socket);
            }
        }
    }

    /**
     * Вызывается из EventLoop как функция вызова на событие таймаута сокета
     */
    public function onTimeout()
    {
        if ($this->exchange) {
            $this->noReadTime += $this->connectionTimeout;
            $this->exchange->onTimeout($this->noReadTime);
        } else {
            Log::log(SERVERLOGNAME, 'ERROR Server Таймаут сокета, для которого не установлен Exchange');
            $this->closeClient('ConnectedStream timeout');
        }
    }

    /**
     * Вызывается из слушающего потока (к которому привязан подключенный поток) на команду закрытия клиента
     */
    public function close($reason = '')
    {
        if ($this->exchange) {
            $this->exchange->onClose($reason);
            $this->exchange = null;
        }

        $this->closeStream();

        $this->listenStream = null;
    }

    /**
     * @inheritedDoc
     */
    public function write($message)
    {
        $socket = $this->socket;

        $this->events->addWriteStream($socket, $this)
            ->removeReadStream($socket);

        $this->stringsToWrite[] = $message;
    }

    /**
     * @inheritedDoc
     */
    public function closeClient($reason)
    {
        $this->exchangeStatus = Exchange::EXCHANGE_STATUS_NOT_ONLINE;
        $this->listenStream->closeClient($reason);
    }

    /**
     * Создание Exchange согласно порту, к которому подключился клиент
     *
     * @return \Inform\Server\Exchange\Exchange
     */
    abstract protected function createExchange(array $exchangeData);
}
