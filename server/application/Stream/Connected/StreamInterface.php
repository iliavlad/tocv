<?php

namespace Inform\Server\Stream\Connected;

/**
 * Работа с потоком со стороны Обмена
 */
interface StreamInterface
{
    /**
     * Закрытие клиента
     *
     * @param string $reason Причина
     */
    public function closeClient($reason);

    /**
     * Запись в поток
     *
     * @param type $message
     */
    public function write($message);
}
