<?php

namespace Inform\Server\Stream\Connected;

/**
 * Работа с Обменом
 */
interface DriverInterface extends StreamInterface
{
    /**
     * Установка статус Обмена
     *
     * @param string $status
     */
    public function setExchangeStatus($status);
}
