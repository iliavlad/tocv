<?php

namespace Inform\Server\Stream\Connected;

use Inform\Server\Exchange\Driver as ExchangeDriver;
use Inform\Server\Exchange\Exchange;

/**
 * Подключённый поток для работы с приборами
 */
class Driver extends Stream implements DriverInterface
{
    /**
     * Создание обмена
     *
     * @param array $exchangeData Данные Обмена
     * @return ExchangeDriver
     */
    protected function createExchange(array $exchangeData)
    {
        return new ExchangeDriver($this, $exchangeData);
    }

    /**
     * @inheritedDoc
     */
    public function setExchangeStatus($status)
    {
        $this->exchangeStatus = $status;
        $this->exchangeStatusUpdated = time();
    }

    /**
     * Получение статуса Обмена
     *
     * @return string
     */
    public function getClientStatus()
    {
        return $this->exchangeStatus;
    }

    /**
     * Передача команды клиенту
     *
     * @param array $parts Команда
     */
    public function sendCommandToClient(array $parts)
    {
        if (Exchange::EXCHANGE_STATUS_ONLINE == $this->exchangeStatus) {
            $this->exchange->processCommand($parts);
        }
    }
}
