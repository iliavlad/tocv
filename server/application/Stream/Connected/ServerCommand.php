<?php

namespace Inform\Server\Stream\Connected;

use Inform\Server\Server\CommandInterface;
use Inform\Server\Loop\StreamInterface as LoopStreamInterface;
use Inform\Server\Stream\Listen\StreamInterface as ListenStreamInterface;
use Inform\Server\Exchange\ServerCommand as Exchange;

/**
 * Подключённый поток для работы с командами сервера
 */
class ServerCommand extends Stream
{
    /**
     * Сервер
     *
     * @var CommandInterface 
     */
    private $serverCommand;

    /**
     *
     * @param LoopStreamInterface $events
     * @param resource $socket
     * @param ListenStreamInterface $listenStream
     * @param CommandInterface $serverCommand
     */
    public function __construct(
        LoopStreamInterface $events,
        $socket,
        ListenStreamInterface $listenStream,
        array $exchangeData,
        CommandInterface $serverCommand)
    {
        $this->serverCommand = $serverCommand;

        parent::__construct($events, $socket, $listenStream, $exchangeData);
    }

    public function __destruct()
    {
        $this->serverCommand = null;

        parent::__destruct();
    }

    /**
     * Создание Exchange для работы с командами сервера
     *
     * @return \Inform\Server\Exchange\ServerCommand
     */
    protected function createExchange(array $exchangeData)
    {
        return new Exchange($this, $exchangeData, $this->serverCommand);
    }
}
