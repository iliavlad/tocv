<?php

namespace Inform\Server\Stream;

/**
 * Коллбеки на соответствующие события
 */
interface ListenerInterface
{
    /**
     * Коллбек на событие чтения
     */
    public function onReadData();

    /**
     * Коллбек на событие записи
     */
    public function onWriteData();

    /**
     * Коллбек на событие тайаута
     */
    public function onTimeout();
}
