<?php

namespace Inform\Server\Stream;

use Inform\Server\Loop\StreamInterface as LoopStreamInterface;

/**
 * Класс потока для связи сокетов и событий, которые возникают у них
 */
class Stream implements ListenerInterface
{
    /**
     * Работы с событиями
     *
     * @var LoopStreamInterface
     */
    protected $events;

    /**
     * Сокет
     *
     * @var resource
     */
    protected $socket;

    public function __construct(LoopStreamInterface $events)
    {
        $this->events = $events;
    }

    public function __destruct()
    {
        $this->closeStream();

        $this->events = null;
        $this->socket = null;
    }

    /**
     * @inheritedDoc
     */
    public function onReadData()
    {
    }

    /**
     * @inheritedDoc
     */
    public function onWriteData()
    {
    }

    /**
     * @inheritedDoc
     */
    public function onTimeout()
    {
    }

    /**
     * Закрытие потока
     *  Отменяем слежение за событиями, закрываем сокет
     *
     * @param resource $stream
     */
    public function closeStream()
    {
        if ($this->events) {
            $this->events->removeReadStream($this->socket);
            $this->events->removeWriteStream($this->socket);
        }

        $this->closeSocket($this->socket);
    }

    /**
     * Закрытие сокета
     *
     * @param resource $socket
     */
    protected function closeSocket($socket)
    {
        if (is_resource($socket)) {
            // Подавление "Transport endpoint is not connected" в php_errors.log
            // при чтении с сообщением "Connection reset by peer"
            @stream_socket_shutdown($socket, STREAM_SHUT_RDWR);
            stream_set_blocking($socket, false);
            fclose($socket);
        }
    }
}
