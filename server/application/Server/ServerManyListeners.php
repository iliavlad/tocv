<?php

namespace Inform\Server\Server;

use Inform\Server\Loop\CommandInterface as LoopCommandInterface;
use Inform\Server\Loop\StreamInterface as LoopStreamInterface;
use Inform\Server\Stream\Listen\ServerCommand as ListenServerCommandStream;
use Inform\Server\Server\Commands;
use Inform\ExchangeConstants;
use Inform\Server\Log;

/**
 * Сервер на расширении event http://docs.php.net/manual/ru/book.event.php
 * Установка должна быть с namespace ExtEvent
 */
class ServerManyListeners implements CommandInterface
{
    use Commands;

    /**
     * Обработка событий
     *
     * @var LoopCommandInterface
     */
    private $loop;

    /**
     * Работа с потоками
     *
     * @var DriverCollectionInterface
     */
    private $streamCollection;

    /**
     * Поток, который ожидает клиента с командами для сервера
     *
     * @var ListenServerCommandStream
     */
    private $listenServerCommandStream;

    /**
     * Порт для приёма команд сервера
     *
     * @var int
     */
    private $serverCommandPort = '';

    /**
     * Адрес, на который идёт подключение слушающих сокетов
     *   Сохраняем для использования в командах
     *
     * @var string ip
     */
    private $serverAddress = '';

    /**
     *
     * @param LoopCommandInterface $loop
     * @param DriverCollectionInterface $streamCollection
     */
    public function __construct(LoopCommandInterface $loop, DriverCollectionInterface $streamCollection)
    {
        $this->loop = $loop;
        $this->streamCollection = $streamCollection;
    }

    /**
     * Старт сервера
     *
     * @param LoopStreamInterface $loop Работа с событиями
     * @param array $ports Порты для подключения
     * @param string $serverAddress адрес сервера
     * @param int $serverPort Порт сервера
     */
    public function start(LoopStreamInterface $loop, array $ports, $serverAddress, $serverPort)
    {
        Log::log(SERVERLOGNAME,
            'Server start serverAddress ' . $serverAddress . ' server port ' . $serverPort);

        try {
            $this->listenServerCommandStream = new ListenServerCommandStream(
                $loop, $serverAddress, $serverPort, $this);

            $this->streamCollection->updatePorts($serverAddress, $ports);

            $this->serverAddress = $serverAddress;
            $this->serverCommandPort = $serverPort;

            $this->loop->mainCycle();
        } catch (\Exception $e) {
            Log::log(SERVERLOGNAME, 'ERROR Server ' . $e->getMessage());
        }

        $this->closeAllClients();

        Log::log(SERVERLOGNAME, 'Server finish');
    }

    /**
     * @inheritedDoc
     */
    public function processCommand(array $parts)
    {
        $result = 'Ошибка';

        $exchange_command = array(
            ExchangeConstants::COMMAND_READ_ARCHIVE,
            ExchangeConstants::COMMAND_READ_CURRENT,
            ExchangeConstants::COMMAND_READ_SETTINGS,
            ExchangeConstants::COMMAND_SAVE_SETTINGS,
        );
        $server_command = array(
            ExchangeConstants::COMMAND_CHECK_STATUS,
            ExchangeConstants::COMMAND_STOP_LOGS,
            ExchangeConstants::COMMAND_CLOSE_PORT,
            ExchangeConstants::COMMAND_UPDATE_PORTS,
            ExchangeConstants::COMMAND_STOP_SERVER,
            ExchangeConstants::COMMAND_CLIENT_COUNT,
            ExchangeConstants::COMMAND_STOP_LISTEN,
        );

        $command = $parts['command'];
        if (in_array($command, $exchange_command)) {
            $this->streamCollection->sendCommandToClient($parts['client_id'], $parts);
            $result = $this->getClientStatus($parts['client_id']);
        } else if (in_array($command, $server_command)) {
            $method = 'serverCommands' . $parts['command'];
            $result = $this->$method($parts);
        }

        Log::log(SERVERLOGNAME, 'Server processCommand. ' . implode('#', $parts) . ". Результат $result.");

        return $result;
    }

    /**
     * Закрытие всех созданных сокетов
     */
    private function closeAllClients()
    {
        Log::log(SERVERLOGNAME, 'Server closeAllClients');

        $ports = $this->streamCollection->getListenPorts();

        foreach ($ports as $port) {
            $this->streamCollection->stopListenByPort($port, 'Закрытие всех');
        }

        $this->streamCollection = null;

        $this->listenServerCommandStream->closeClient('Закрытие всех');
        $this->listenServerCommandStream->closeStream();
        $this->listenServerCommandStream = null;
    }
}
