<?php

namespace Inform\Server\Server;

use Inform\Server\Loop\StreamInterface;
use Inform\Server\Exchange\Exchange;
use Inform\Server\Stream\Listen\Driver as ListenDriverStream;

/**
 * Коллекция слушающих потоков, которые работают с приборами
 */
class DriverCollection implements DriverCollectionInterface
{
    /**
     * Работа с событиями
     *
     * @var StreamInterface
     */
    private $events;

    /**
     * Массив потоков
     *   Ключ - порт, значение - слушающих поток
     *
     * @var array of ListenStreamConnectedInterface
     */
    private $listenStreamByPort = [];

    /**
     *
     * @param StreamInterface $events Работа с событиями
     */
    public function __construct(StreamInterface $events)
    {
        $this->events = $events;
    }

    public function __destruct()
    {
        foreach ($this->listenStreamByPort as &$stream) {
            $stream = null;
        }

        $this->events = null;
    }

    /**
     * @inheritedDoc
     */
    public function updatePorts($serverAddress, array $ports)
    {
        foreach ($ports as $port) {
            $stream = new ListenDriverStream($this->events, $serverAddress, $port);

            $this->listenStreamByPort[$port] = $stream;

            \Inform\Server\Log::log(SERVERLOGNAME, "Server stream_socket_server port $port OK");
        }
    }

    /**
     * @inheritedDoc
     */
    public function getListenPorts()
    {
        return array_keys($this->listenStreamByPort);
    }

    /**
     * @inheritedDoc
     */
    public function getClientStatusByPort($port)
    {
        $status = Exchange::EXCHANGE_STATUS_NOT_LISTEN;

        if (isset($this->listenStreamByPort[$port])) {
            $status = $this->listenStreamByPort[$port]->getClientStatus();
        }

        return $status;
    }

    /**
     * @inheritedDoc
     */
    public function closeClientByPort($port, $reason = '')
    {
        if (isset($this->listenStreamByPort[$port])) {
            $this->listenStreamByPort[$port]->closeClient($reason);
        }
    }

    /**
     * @inheritedDoc
     */
    public function stopListenByPort($port, $reason = '')
    {
        if (isset($this->listenStreamByPort[$port])) {
            $this->listenStreamByPort[$port]->closeClient($reason);
            $this->listenStreamByPort[$port]->closeStream();
            $this->listenStreamByPort[$port] = null;
            unset($this->listenStreamByPort[$port]);
        }
    }

    /**
     * @inheritedDoc
     */
    public function sendCommandToClient($port, array $parts)
    {
        if (isset($this->listenStreamByPort[$port])) {
            $this->listenStreamByPort[$port]->sendCommandToClient($parts);
        }
    }

    /**
     * @inheritedDoc
     */
    public function getConnectedClientCount()
    {
        $count = 0;

        foreach ($this->listenStreamByPort as $stream) {
            if (Exchange::EXCHANGE_STATUS_NOT_ONLINE != $stream->getClientStatus()) {
                $count++;
            }
        }

        return $count;
    }
}
