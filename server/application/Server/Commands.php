<?php

namespace Inform\Server\Server;

use Inform\Server\Database\Database;
use Inform\Server\Log;

/**
 * Команды сервера
 */
trait Commands
{
    /**
     * Остановка сервера
     *
     * @param array $parts Данные команды
     * @return string
     */
    private function serverCommandsStopServer($parts)
    {
        $this->loop->stop();

        return 'closed';
    }

    /**
     * Остановка логов
     *   При ротации логов они перемещаются в отдельную папку для бекапа
     *   При этом открытый файл надо закрыть и создать новый в папке с логами сервера
     *
     * @param array $parts Данные команды
     * @return string
     */
    private function serverCommandsStopLogs($parts)
    {
        Log::stopAll();

        return 'logs stopped';
    }

    /**
     * Добавление портов для прослушивания сервером
     *
     * @param array $parts Данные команды
     * @return string
     */
    private function serverCommandsUpdatePorts($parts)
    {
        $o_db = Database::getInstanse();
        if (DEV_PORT != $parts['client_id']) {
            $allPorts = $o_db->getPortsToConnect();
        } else {
            $allPorts = $o_db->getPortsToConnectNewDevice();
        }

        $ports = array_diff($allPorts, $this->streamCollection->getListenPorts());
        try {
            $this->streamCollection->updatePorts($this->serverAddress, $ports);
        } catch (Exception $e) {
            Log::log(SERVERLOGNAME, 'ERROR Server ' . $e->getMessage());
        }

        return 'updated';
    }

    /**
     * Получение данных о количестве подключённых клиентов
     *
     * @param array $parts Данные команды
     * @return int
     */
    private function serverCommandsClientCount($parts)
    {
        return $this->streamCollection->getConnectedClientCount();
    }

    /**
     * Получение статуса клиента
     *
     * @param array $parts Данные команды
     * @return string
     */
    private function serverCommandsCheckStatus($parts)
    {
        return $this->getClientStatus($parts['client_id']);
    }

    /**
     * Закрытие порта клиента
     *   Нужна возможность прерывать передачу информацию от клиента
     *
     * @param array $parts Данные команды
     * @return string
     */
    private function serverCommandsClosePort($parts)
    {
        if ($this->serverCommandPort != $parts['client_id']) {
            $this->streamCollection->closeClientByPort($parts['client_id'],
                'команда сервера закрыть порт');
        }

        return $this->getClientStatus($parts['client_id']);
    }

    /**
     * Остановка прослушивания порта
     *
     * @param array $parts Данные команды
     * @return string
     */
    private function serverCommandsStopListen($parts)
    {
        if ($this->serverCommandPort != $parts['client_id']) {
            $this->streamCollection->stopListenByPort($parts['client_id'],
                'команда сервера остановить прослушивание');
        }

        return $this->getClientStatus($parts['client_id']);
    }
    /**
     * Получение статуса клиента
     *
     * @param string $port Порт клиента
     * @return string
     */
    private function getClientStatus($port)
    {
        $status = \Inform\Server\Exchange\Exchange::EXCHANGE_STATUS_ONLINE;

        if ($this->serverCommandPort != $port) {
            $status = $this->streamCollection->getClientStatusByPort($port);
        }

        return $status;
    }
}
