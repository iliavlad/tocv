<?php

namespace Inform\Server\Server;

/**
 * Работа с командами сервера
 */
interface CommandInterface
{
    /**
     * Обработка команды для сервера
     *
     * @param array $parts Команда
     */
    public function processCommand(array $parts);
}
