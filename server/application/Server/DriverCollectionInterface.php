<?php

namespace Inform\Server\Server;

/**
 * Работа со списком потоков, связанных с приборами
 */
interface DriverCollectionInterface
{
    /**
     * Добавить новые порты в список для прослушивания
     *
     * @param string $serverAddress
     * @param array $ports
     */
    public function updatePorts($serverAddress, array $ports);

    /**
     * Получение списка прослушиваемых портов
     *
     * @return array
     */
    public function getListenPorts();

    /**
     * Получение статуса клиента по порту
     *
     * @param string $port
     * @return string
     */
    public function getClientStatusByPort($port);

    /**
     * Закрытие клиента по порту
     *  Например, для прерывания передачи данных
     *
     * @param string $port Порт
     * @param string $reason Причина
     */
    public function closeClientByPort($port, $reason = '');

    /**
     * Остановка прослушивания порта
     *
     * @param string $port Порт
     * @param string $reason Причина
     */
    public function stopListenByPort($port, $reason = '');

    /**
     * Отправка команды клиенту
     *
     * @param string $port Порт
     * @param array $parts Команда
     */
    public function sendCommandToClient($port, array $parts);

    /**
     * Получение количества подключённых клиентов
     *
     * @return int
     */
    public function getConnectedClientCount();
}
